package com.demaciandestroyers.themisplugin.trivia;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Map.Entry;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
import org.pircbotx.hooks.Event;
import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.MessageEvent;

import com.demaciandestroyers.themiscore.plugins.EventInterface;

public class Trivia extends ListenerAdapter implements EventInterface {

	private static final int LOBBY_TIMEOUT = 90;
	private static final int QUESTION_TIMEOUT = 30;
	private static final boolean SLOW_MODE = true;
	private static final int SLOW_MODE_TIME = 30;
	private boolean lobby_open = true;
	private boolean question_open = true;
	private boolean trivia_open = true;
	private SessionFactory factory;
	private ServiceRegistry serviceRegistry;
	private String anwser;
	private String question;
	private Timer timer;
	private int questions;
	private String topic;
	private int counter = 0;
	private HashMap<String, Integer> players = new HashMap<String, Integer>();
	private MessageEvent eventIn;

	public void addPoint(String player) {
		if (this.players.containsKey(player)) {
			int temp = this.players.get(player);
			this.players.put(player, temp + 1);
		}
	}

	public String getWinner() {
		String winner = ""; //$NON-NLS-1$
		int maxValue = 0;
		Set<Entry<String, Integer>> temp = this.players.entrySet();
		Iterator<Entry<String, Integer>> it = temp.iterator();
		while (it.hasNext()) {
			Map.Entry<String, Integer> pairs = it.next();
			if (pairs.getValue() > maxValue) {
				maxValue = pairs.getValue();
				winner = pairs.getKey() + "with " + maxValue + " points!"; //$NON-NLS-1$//$NON-NLS-2$
			}
		}
		return winner;
	}

	public String getQuestion() {
		return this.question;
	}

	public String getAnwser() {
		return this.anwser;
	}

	public void addPlayer(String player) {
		if (!this.players.containsKey(player)) {
			this.players.put(player, 0);
		}
	}

	public void selectQuestion() {
		Session session = this.factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			@SuppressWarnings("rawtypes")
			List customers = session
					.createQuery(
							"Select question,type FROM trivia WHERE topic = " + this.topic + " ORDER BY rand( ) LIMIT 1").list(); //$NON-NLS-1$ //$NON-NLS-2$
			for (@SuppressWarnings("rawtypes")
			Iterator iterator = customers.iterator(); iterator.hasNext();) {
				TriviaBean tb = (TriviaBean) iterator.next();
				this.question = tb.getQuestion();
				this.anwser = tb.getAnwser();
				this.question_open = true;
			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}

	}

	public void startGame(@SuppressWarnings("hiding") int questions,
			@SuppressWarnings("hiding") String topic) {
		Configuration configuration = new Configuration();
		configuration.configure();
		this.serviceRegistry = new ServiceRegistryBuilder().applySettings(
				configuration.getProperties()).buildServiceRegistry();
		this.factory = configuration.buildSessionFactory(this.serviceRegistry);
		this.timer = new Timer();
		this.questions = questions;
		this.topic = topic;
		this.timer.schedule(new TimerTask() {
			@Override
			public void run() {
				Trivia.this.setLobby_open(false);
			}
		}, LOBBY_TIMEOUT * 1000);

	}

	public static boolean isSlowMode() {
		return SLOW_MODE;
	}

	public static int getSlowModeTime() {
		return SLOW_MODE_TIME;
	}

	public boolean isLobby_open() {
		return this.lobby_open;
	}

	public void setLobby_open(boolean lobby_open) {
		this.lobby_open = lobby_open;
	}

	public boolean isQuestion_open() {
		return this.question_open;
	}

	public void setQuestion_open(boolean question_open) {
		this.question_open = question_open;
	}

	public int getQuestions() {
		return this.questions;
	}

	public int getCounter() {
		return this.counter;
	}

	public static int getQuestionTimeout() {
		return QUESTION_TIMEOUT;
	}

	public static int getLobbyTimeout() {
		return LOBBY_TIMEOUT;
	}

	public boolean isTrivia_open() {
		return this.trivia_open;
	}

	public void setTrivia_open(boolean trivia_open) {
		this.trivia_open = trivia_open;
	}

	public void onEvent(Event rawevent) throws Exception {
		// Since we extend ListenerAdapter and implement Listener in the same
		// class
		// call the super onEvent so ListenerAdapter will work
		// Unless you are doing that, this line shouldn't be added
		super.onEvent(rawevent);

		// Make sure were dealing with a message
		if (rawevent instanceof MessageEvent) {
			// Cast to get access to all the MessageEvent specific methods
			MessageEvent event = (MessageEvent) rawevent;
			this.eventIn = event;
			// Check Input
			String[] split = event.getMessage().split(" "); //$NON-NLS-1$
			if (event.getMessage().startsWith("!trivia")) { //$NON-NLS-1$
				event.getBot().sendMessage(
						event.getChannel(),Messages.getString("Trivia.3")); //$NON-NLS-1$
				event.getBot().sendMessage(
						event.getChannel(),Messages.getString("Trivia.4")); //$NON-NLS-1$
				event.getBot().sendMessage(
						event.getChannel(),Messages.getString("Trivia.5")); //$NON-NLS-1$
			} else if (event.getMessage().startsWith("!ans")) { //$NON-NLS-1$
				String userAns = event.getMessage().replace("!ans ", ""); //$NON-NLS-1$ //$NON-NLS-2$
				if (userAns.equalsIgnoreCase(this.getAnwser())) {
					if (this.isQuestion_open()) {
						this.addPoint(event.getUser().getNick());
						event.getBot().sendMessage(
								event.getChannel(),
								event.getUser().getNick()
										+ Messages.getString("Trivia.0")); //$NON-NLS-1$
						this.setQuestion_open(false);
					}
				}
			} else if (event.getMessage().startsWith("!join-trivia")) { //$NON-NLS-1$
				if (this.isLobby_open()) {
					this.addPlayer(event.getUser().getNick());
					event.getBot().sendMessage(
							event.getChannel(),
							event.getUser().getNick()
									+ Messages.getString("Trivia.1")); //$NON-NLS-1$
				} else {
					event.getBot().sendMessage(event.getChannel(),
							Messages.getString("Trivia.2")); //$NON-NLS-1$
				}
			} else if (event.getMessage().startsWith("!start-trivia")) { //$NON-NLS-1$
				startGame(Integer.valueOf(split[1]), split[2]);
				Timer timer2 = new Timer();
				timer2.scheduleAtFixedRate(
						new TimerTask() {
							public void run() {
								if (Trivia.this.getCounter() < Trivia.this
										.getQuestions()) {
									Trivia.this.eventIn
											.getBot()
											.sendMessage(
													Trivia.this.eventIn
															.getChannel(),
													Messages.getString("Trivia.6") + Trivia.this.getAnwser());  //$NON-NLS-1$
									Trivia.this.selectQuestion();
									Trivia.this.eventIn.getBot().sendMessage(
											Trivia.this.eventIn.getChannel(),
											Messages.getString("Trivia.7")); //$NON-NLS-1$
									Trivia.this.eventIn.getBot().sendMessage(
											Trivia.this.eventIn.getChannel(),
											Trivia.this.getQuestion());
								} else {
									Trivia.this.eventIn
											.getBot()
											.sendMessage(
													Trivia.this.eventIn
															.getChannel(),
													Messages.getString("Trivia.8"));  //$NON-NLS-1$
									Trivia.this.eventIn
											.getBot()
											.sendMessage(
													Trivia.this.eventIn
															.getChannel(),
													Messages.getString("Trivia.9") + Trivia.this.getWinner());  //$NON-NLS-1$
									Trivia.this.setTrivia_open(false);
									this.cancel();
								}
							}
						}, Trivia.getLobbyTimeout() * 1000,
						Trivia.getQuestionTimeout() * 1000);
			}

		}
	}

}
