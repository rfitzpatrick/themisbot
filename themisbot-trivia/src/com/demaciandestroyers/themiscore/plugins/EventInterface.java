package com.demaciandestroyers.themiscore.plugins;

import org.pircbotx.hooks.Event;
import org.pircbotx.hooks.Listener;

import net.xeoh.plugins.base.Plugin;

public interface EventInterface extends Plugin{
	public void onEvent(Event rawevent) throws Exception;

}
