It is currently {weather.current_condition.temp_C} C / {weather.current_condition.temp_F} F  and {weather.current_condition.weatherDesc} in {weather.request.query}.
There is a {weather.current_condition.winddirDegree} wind at {weather.current_condition.windspeedMiles} MPH. Visibility is currently{weather.current_condition.visibility}%.
Current precipitation is {weather.current_condition.precipMM} MM
Today should be {weather.weather.weatherDesc} with possible precipitation at {weather.weather.precipMM} MM
Todays High is {weather.weather.tempMaxC} C / {weather.weather.tempMaxF} F and Today's low is {weather.weather.tempMinC} C / {weather.weather.tempMinF} F.