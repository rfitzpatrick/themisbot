package com.demaciandestroyers.themiscore.plugins.weather.xml;

public class WeatherForcast {

	String date;
	float precipMM;
	float tempMaxC;
	float tempMinC;
	float tempMaxF;
	float tempMinF;
	int weatherCode;
	String weatherDesc;
	String weatherIconUrl;
	String winddir16Point;
	int winddirDegree;
	String winddirection;
	float windspeedKmph;
	float windspeedMiles;
	public String getDate() {
		return this.date;
	}
	public float getprecipMM() {
		return this.precipMM;
	}
	public float getTempMaxC() {
		return this.tempMaxC;
	}
	public float getTempMinC() {
		return this.tempMinC;
	}
	public float getTempMaxF() {
		return this.tempMaxF;
	}
	public float getTempMinF() {
		return this.tempMinF;
	}
	public int getWeatherCode() {
		return this.weatherCode;
	}
	public String getWeatherDesc() {
		return this.weatherDesc;
	}
	public String getWeatherIconUrl() {
		return this.weatherIconUrl;
	}
	public String getWinddir16Point() {
		return this.winddir16Point;
	}
	public int getWinddirDegree() {
		return this.winddirDegree;
	}
	public String getWinddirection() {
		return this.winddirection;
	}
	public float getWindspeedKmph() {
		return this.windspeedKmph;
	}
	public float getWindspeedMiles() {
		return this.windspeedMiles;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public void setprecipMM(float precipMM) {
		this.precipMM = precipMM;
	}
	public void setTempMaxC(float tempMaxC) {
		this.tempMaxC = tempMaxC;
	}
	public void setTempMinC(float tempMinC) {
		this.tempMinC = tempMinC;
	}
	public void setTempMaxF(float tempMaxF) {
		this.tempMaxF = tempMaxF;
	}
	public void setTempMinF(float tempMinF) {
		this.tempMinF = tempMinF;
	}
	public void setWeatherCode(int weatherCode) {
		this.weatherCode = weatherCode;
	}
	public void setWeatherDesc(String weatherDesc) {
		this.weatherDesc = weatherDesc;
	}
	public void setWeatherIconUrl(String weatherIconUrl) {
		this.weatherIconUrl = weatherIconUrl;
	}
	public void setWinddir16Point(String winddir16Point) {
		this.winddir16Point = winddir16Point;
	}
	public void setWinddirDegree(int winddirDegree) {
		this.winddirDegree = winddirDegree;
	}
	public void setWinddirection(String winddirection) {
		this.winddirection = winddirection;
	}
	public void setWindspeedKmph(float windspeedKmph) {
		this.windspeedKmph = windspeedKmph;
	}
	public void setWindspeedMiles(float windspeedMiles) {
		this.windspeedMiles = windspeedMiles;
	}
}

