package com.demaciandestroyers.themiscore.plugins.weather.xml;

public class Location {
String query;
String type;
public String getQuery() {
	return this.query;
}
public String getType() {
	return this.type;
}
public void setQuery(String query) {
	this.query = query;
}
public void setType(String type) {
	this.type = type;
}
}
