package com.demaciandestroyers.themiscore.plugins.weather.xml;

public class DataTimeZone {
Location request;
TimeZone time_zone;
public Location getRequest() {
	return this.request;
}
public TimeZone getTime_zone() {
	return this.time_zone;
}
public void setRequest(Location request) {
	this.request = request;
}
public void setTime_zone(TimeZone time_zone) {
	this.time_zone = time_zone;
}
}
