package com.demaciandestroyers.themiscore.plugins.weather.xml;

public class CurrentCondition {

	int cloudcover;
	int humidity;
	String observation_time;
	float precipMM;
	int pressure;
	float temp_C;
	float temp_F;
	int visibility;
	int weatherCode;
	String weatherDesc;
	String weatherIconUrl;
	String winddir16Point;
	int winddirDegree;
	float windspeedKmph;
	float windspeedMiles;
	public int getCloudcover() {
		return this.cloudcover;
	}
	public int getHumidity() {
		return this.humidity;
	}
	public String getObservation_time() {
		return this.observation_time;
	}
	public float getprecipMM() {
		return this.precipMM;
	}
	public int getPressure() {
		return this.pressure;
	}
	public float getTemp_C() {
		return this.temp_C;
	}
	public float getTemp_F() {
		return this.temp_F;
	}
	public int getVisibility() {
		return this.visibility;
	}
	public int getWeatherCode() {
		return this.weatherCode;
	}
	public String getWeatherDesc() {
		return this.weatherDesc;
	}
	public String getWeatherIconUrl() {
		return this.weatherIconUrl;
	}
	public String getWinddir16Point() {
		return this.winddir16Point;
	}
	public int getWinddirDegree() {
		return this.winddirDegree;
	}
	public float getWindspeedKmph() {
		return this.windspeedKmph;
	}
	public float getWindspeedMiles() {
		return this.windspeedMiles;
	}
	public void setCloudcover(int cloudcover) {
		this.cloudcover = cloudcover;
	}
	public void setHumidity(int humidity) {
		this.humidity = humidity;
	}
	public void setObservation_time(String observation_time) {
		this.observation_time = observation_time;
	}
	public void setprecipMM(float precipMM) {
		this.precipMM = precipMM;
	}
	public void setPressure(int pressure) {
		this.pressure = pressure;
	}
	public void setTemp_C(float temp_C) {
		this.temp_C = temp_C;
	}
	public void setTemp_F(float temp_F) {
		this.temp_F = temp_F;
	}
	public void setVisibility(int visibility) {
		this.visibility = visibility;
	}
	public void setWeatherCode(int weatherCode) {
		this.weatherCode = weatherCode;
	}
	public void setWeatherDesc(String weatherDesc) {
		this.weatherDesc = weatherDesc;
	}
	public void setWeatherIconUrl(String weatherIconUrl) {
		this.weatherIconUrl = weatherIconUrl;
	}
	public void setWinddir16Point(String winddir16Point) {
		this.winddir16Point = winddir16Point;
	}
	public void setWinddirDegree(int winddirDegree) {
		this.winddirDegree = winddirDegree;
	}
	public void setWindspeedKmph(float windspeedKmph) {
		this.windspeedKmph = windspeedKmph;
	}
	public void setWindspeedMiles(float windspeedMiles) {
		this.windspeedMiles = windspeedMiles;
	}
}
