package com.demaciandestroyers.themiscore.plugins.weather;


import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.net.MalformedURLException;

import org.pircbotx.hooks.Event;
import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.MessageEvent;

import com.demaciandestroyers.themiscore.plugins.EventInterface;
import com.demaciandestroyers.themiscore.plugins.weather.xml.DataTimeZone;
import com.demaciandestroyers.themiscore.plugins.weather.xml.DataWeather;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.SimpleHash;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;
import freemarker.template.Version;

public class Weather extends ListenerAdapter implements EventInterface{
	
	private String loc = "02809"; //$NON-NLS-1$
	private final static String API_KEY = "26qu4dr7rvqrn59bnuapzssm"; //$NON-NLS-1$
	private final static String URL = "http://api.worldweatheronline.com/free/v1/weather.ashx?q=$1&format=xml&num_of_days=1&key=$2"; //$NON-NLS-1$
	private final static String URL_TIMEZONE = "http://api.worldweatheronline.com/free/v1/tz.ashx?q=$1&format=xml&key=$2"; //$NON-NLS-1$
	XStream xStreamWeather;
	XStream xStreamTimezone;
	DataWeather data1;
	DataTimeZone data2;
	Configuration cfg = new Configuration();

	public Weather() throws IOException{
		 
        /* ----------------------------------------------------------------------- */    
        /* You should do this ONLY ONCE in the whole application life-cycle:       */    
    
        /* Create and adjust the configuration */
       
        this.cfg.setDirectoryForTemplateLoading(new File("templates/")); //$NON-NLS-1$
        this.cfg.setObjectWrapper(new DefaultObjectWrapper());
        this.cfg.setDefaultEncoding("UTF-8"); //$NON-NLS-1$
        this.cfg.setTemplateExceptionHandler(TemplateExceptionHandler.HTML_DEBUG_HANDLER);
        this.cfg.setIncompatibleImprovements(new Version(2, 3, 20));

        /* ----------------------------------------------------------------------- */    
		this.xStreamWeather = new XStream(new DomDriver());
		this.xStreamTimezone = new XStream(new DomDriver());
		//Set Aliases
		this.xStreamWeather.alias("data", DataWeather.class); //$NON-NLS-1$
		this.xStreamTimezone.alias("data", DataTimeZone.class); //$NON-NLS-1$
	}
	public void setLocation(String loc){
		this.loc= loc;
	}
	
	public void getData(){
		String final_url = URL.replace("$1", this.loc).replace("$2", API_KEY); //$NON-NLS-1$ //$NON-NLS-2$
		String final_url_timezone = URL_TIMEZONE.replace("$1", this.loc).replace("$2", API_KEY); //$NON-NLS-1$ //$NON-NLS-2$
		try {
			this.data1 = (DataWeather)this.xStreamWeather.fromXML(new java.net.URL(final_url));
			this.data2 = (DataTimeZone)this.xStreamTimezone.fromXML(new java.net.URL(final_url_timezone));
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
	
	public DataWeather getWeatherData(){
		return this.data1;
	}
	public DataTimeZone getTimeZone(){
		return this.data2;
	}
	
	public void onEvent(Event rawevent) throws Exception {
		// Since we extend ListenerAdapter and implement Listener in the same
		// class
		// call the super onEvent so ListenerAdapter will work
		// Unless you are doing that, this line shouldn't be added
		super.onEvent(rawevent);

		// Make sure were dealing with a message
		if (rawevent instanceof MessageEvent) {
			// Cast to get access to all the MessageEvent specific methods
			MessageEvent event = (MessageEvent) rawevent;
			String[] split = event.getMessage().split(" "); //$NON-NLS-1$
			if (event.getMessage().startsWith(Messages.getString("Weather.0"))){ //$NON-NLS-1$
				setLocation(split[1]);
				getData();
				SimpleHash root = new SimpleHash();
				root.put("weather", getWeatherData()); //$NON-NLS-1$
				root.put("timezone", getTimeZone()); //$NON-NLS-1$
				Template template = this.cfg.getTemplate("weather.ftl"); //$NON-NLS-1$
				Writer out = new StringWriter();
				template.process(root, out);
				event.getBot().sendMessage(event.getChannel(),
						event.getUser().getNick()
						+ out.toString()); //$NON-NLS-1$
			}
				
			}
			
		}
	}

