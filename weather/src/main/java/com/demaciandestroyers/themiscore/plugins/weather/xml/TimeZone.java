package com.demaciandestroyers.themiscore.plugins.weather.xml;

public class TimeZone {
String localtime;
float utcOffset;
public String getLocaltime() {
	return this.localtime;
}
public float getUtcOffset() {
	return this.utcOffset;
}
public void setLocaltime(String localtime) {
	this.localtime = localtime;
}
public void setUtcOffset(float utcOffset) {
	this.utcOffset = utcOffset;
}
}
