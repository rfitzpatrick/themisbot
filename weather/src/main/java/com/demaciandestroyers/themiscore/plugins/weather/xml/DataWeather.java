package com.demaciandestroyers.themiscore.plugins.weather.xml;

public class DataWeather {
CurrentCondition current_condition;
Location request;
WeatherForcast weather;
public CurrentCondition getCurrent_condition() {
	return this.current_condition;
}
public Location getRequest() {
	return this.request;
}
public WeatherForcast getWeather() {
	return this.weather;
}
public void setCurrent_condition(CurrentCondition current_condition) {
	this.current_condition = current_condition;
}
public void setRequest(Location request) {
	this.request = request;
}
public void setWeather(WeatherForcast weather) {
	this.weather = weather;
}
}
