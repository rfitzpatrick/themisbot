package com.demaciandestroyers.themiscore;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import net.xeoh.plugins.base.PluginManager;
import net.xeoh.plugins.base.impl.PluginManagerFactory;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Logger logger = LoggerFactory.getLogger(Main.class);

		String loadBot;
		ApplicationContext context = null;
		BufferedReader br = null;
		try {
			context = new ClassPathXmlApplicationContext("Beans.xml"); //$NON-NLS-1$ 
			br = new BufferedReader(new InputStreamReader(System.in));
			while (true) {
				System.out
						.println(Messages.getString("Main.1")); //$NON-NLS-1$
				System.out
						.println(Messages.getString("Main.2")); //$NON-NLS-1$
				System.out.println(Messages.getString("Main.3")); //$NON-NLS-1$
				loadBot = br.readLine();
				if (loadBot.equalsIgnoreCase(Messages.getString("Main.0"))) { //$NON-NLS-1$
					break;
				}
				context.getBean(loadBot);
			}
		} catch (IOException e) {
			logger.error(e.toString());
		} finally {
			((ClassPathXmlApplicationContext) context).close();
			try {
				br.close();
			} catch (IOException e) {
				logger.error(e.toString());
			}
		}

	}

}
