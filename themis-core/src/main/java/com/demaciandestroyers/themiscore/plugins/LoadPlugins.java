package com.demaciandestroyers.themiscore.plugins;

import java.io.File;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.xeoh.plugins.base.PluginManager;
import net.xeoh.plugins.base.impl.PluginManagerFactory;
import net.xeoh.plugins.base.util.PluginManagerUtil;


public class LoadPlugins{
	Collection<EventInterface> event;
	PluginManager pm;
	Logger logger = LoggerFactory.getLogger(LoadPlugins.class);
	public LoadPlugins(){		
				
	}
	public void createPluginManager(){
		this.pm = PluginManagerFactory.createPluginManager();
	}
	public void loadListeners(){
		this.pm.addPluginsFrom(new File("plugins/").toURI()); //$NON-NLS-1$
		this.event = new PluginManagerUtil(this.pm).getPlugins(EventInterface.class);
	}
	public void shutdown(){
		this.pm.shutdown();
	}
	public Collection<EventInterface> getEventPlugins(){
		return this.event;
	}
	
	}
	
