package com.demaciandestroyers.themiscore.utils;

import java.util.ArrayList;
import java.util.Collection;
import org.pircbotx.hooks.Listener;
import org.pircbotx.hooks.managers.ListenerManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.demaciandestroyers.themiscore.plugins.EventInterface;
import com.demaciandestroyers.themiscore.plugins.LoadPlugins;

public class ListenerFactory {
	Logger logger = LoggerFactory.getLogger(ListenerFactory.class);
	LoadPlugins lp;
	@SuppressWarnings("rawtypes")
	ListenerManager manager;
	@SuppressWarnings("rawtypes")	
	ArrayList<EventInterface> selectedPlugins;
	public ListenerFactory(LoadPlugins lp, ArrayList<EventInterface> selectedPlugins){
		this.lp = lp;
		this.selectedPlugins = selectedPlugins;
	}
	public void loadPlugins(){
		Collection<EventInterface> plugins = this.lp.getEventPlugins();
		for(EventInterface eventPlugin:this.selectedPlugins){
			if(plugins.contains(eventPlugin)){
				this.manager.addListener((Listener)eventPlugin);
			} else {
				this.logger.error(eventPlugin.toString() + Messages.getString("ListenerFactory.0")); 		 //$NON-NLS-1$
				}
		}
	}
	public ListenerManager getManager(){
		return this.manager;
	}
}
