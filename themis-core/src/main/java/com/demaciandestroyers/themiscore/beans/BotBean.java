package com.demaciandestroyers.themiscore.beans;

import java.io.IOException;
import java.util.Map;

import org.pircbotx.PircBotX;
import org.pircbotx.exception.IrcException;
import org.pircbotx.exception.NickAlreadyInUseException;
import org.pircbotx.hooks.managers.ListenerManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.demaciandestroyers.themiscore.utils.Messages;

public class BotBean {

	PircBotX bot;
	ListenerManager manager;
	String name;
	String url;
	int port;
	String password;
	String channel;
	Logger logger = LoggerFactory.getLogger(BotBean.class);
	
	public BotBean(PircBotX bot, ListenerManager manager, String name, String url, int port, String password, String channel){
		this.bot = bot;
		this.manager = manager;
		this.name = name;
		this.url = url;
		this.port = port;
		this.password = password;
		this.channel = channel;
	}
	
	public void intializeBot(){
		this.bot.setListenerManager(manager);
		this.bot.setName(this.name);
		try {
			this.bot.connect(this.url,this.port,this.password); //$NON-NLS-1$
		} catch (NickAlreadyInUseException e) {
			this.logger.info(Messages.getString("CreateBot.0")); //$NON-NLS-1$
		} catch (IOException e) {
			this.logger.error(e.toString());
		} catch (IrcException e) {
			this.logger.error(e.toString());
		}
		this.bot.joinChannel(this.channel);
	}
}
